function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('fa-plus fa-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);



$(function() {
  $('#Grid').mixitup({
      showOnLoad: 'production'
  });

	$('.navbar-nav a').click(function (e) {
		e.preventDefault();
		$(this).parent('li').toggleClass('active');
		$(this).parent('li').siblings('.active').removeClass('active');
	})

	$('.header .navbar-nav a').smoothScroll();
});


function scrollTo(elem) {
  $('body,html').animate({
    scrollTop: elem.offset().top
  }, 500);
}

$(function() {
  $('#jump2top').css('bottom', '-100px');
  $(window).scroll(function () {
    var btn = $('#jump2top');
    if ($(this).scrollTop() > 100) {
      btn.stop().animate({'bottom' : '0'}, 200);
    } else {
      btn.stop().animate({'bottom' : '-100px'}, 200);
  	}
  });

  $('#jump2top').smoothScroll();
});

$(function(){
	$('.error .success').hide();
	$('#button-send').click(function(event){
		$('#button-send').html('Sending Message...');
		event.preventDefault();

		$.ajax({
			type: 'POST',
			url: 'send-message/',
			data: $('#contact_form').serialize(),
			success: function(html) {
				if(html.success == '1')
				{
					$('#button-send').html('Send Message Now');
					$('#success').show();
				}
				else
				{
					$('#button-send').html('Send Message Now');
					$('#error').show();
				}
			},
			error: function(){
				$('#button-send').html('Send Message Now');
				$('#error').show();
			}
		});

	});
});

// Cache selectors
var lastId,
 topMenu = $("#navig"),
 topMenuHeight = topMenu.outerHeight()+1,
 // All list items
 menuItems = topMenu.find("a"),
 // Anchors corresponding to menu items
 scrollItems = menuItems.map(function(){
   var item = $($(this).attr("href"));
    if (item.length) { return item; }
 });

// adding scroll spy
 $(window).scroll(function(){
   // Get container scroll position
   var fromTop = $(this).scrollTop()+topMenuHeight;

   // Get id of current scroll item
   var cur = scrollItems.map(function(){
     if ($(this).offset().top < fromTop)
       return this;
   });
   // Get the id of the current element
   cur = cur[cur.length-1];
   var id = cur && cur.length ? cur[0].id : "";

   if (lastId !== id) {
       lastId = id;
       // Set/remove active class
       menuItems
         .parent().removeClass("active active-item")
         .end().filter("[href=#"+id+"]").parent().addClass("active active-item");
   }
});
