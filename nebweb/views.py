from django.shortcuts import render, redirect
import json
from django.core import mail
from django.http import HttpResponse


def index(request):
    context = {}
    return render(request, 'nebweb/index.html', context=context)

def send_message(request):
    message = {}
    try:
        mail.send_mail(request.POST.get('subject'),
                       (request.POST.get('message') + '\n'+
                       'Sender: '+ request.POST.get('email') + '\n' +
                       'Name: ' + request.POST.get('name')),
                       'qurban.ali@iceanimations.net',
                       ['talha.ahmed@gmail.com'],
                       fail_silently=False)
    except Exception as ex:
        print (str(ex))
        message['success'] = 0
    else:
        message['success'] = 1
    return HttpResponse(json.dumps(message), content_type='application/json')
